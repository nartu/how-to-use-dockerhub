# Docker Hub

0. В этом уроке разберёмся что такое Docker Hub и как им пользоваться. Вообще говоря если мы пользуемся Docker, то докер хаб автоматически тоже, т.к это хранилище образов по умолчанию и именно оттуда подтягиваются образы когда мы даём команду `docker pull` или `docker run` и запускаем контейнеры для своих целей. Так же в Docker Hub можно хранить свои образы, при чём совершенно бесплатно и практически неограниченно. Но давайте обо всём по порядку.

1. Если докер ещё не скачен и установлен, то для начала стоит это сделать.
В этом уроке мы не будем подробно останавливаться на установке, посмотреть как это делается можно в официальной документации. Нужно просто копировать команды написанные там.
https://docs.docker.com/engine/install/ubuntu/
https://docs.docker.com/compose/install/
Проверяем установлен ли докер `docker --version`.

2. Теперь запускаем контейнер из приветственный образ
```
docker pull hello-world
docker run hello-world
```
И мы не только видим что всё работает, а докер пишет нам что происходит.
Главная мысль что мы даём команду взять образ из некого Docker Hub, на основании этого образа создаём контейнер, который отрабатывает и в данном случае пишет нам некий (тот самый) текст в терминал. Так же дана ссылка на хаб https://hub.docker.com/. По умолчанию все образы подгружаются к нам оттуда.
Команда `docker pull` не обязательна, она применяется автоматически при запуске `docker run`, но можно разделить эти операции. А так же `docker pull` нужна чтобы обновить образ, т.к `docker run` использует локальный образ, если он существует, а он может быть устаревшим.

3. Давайте попробуем зайти на этот сайт. Здесь можно искать образы, и хранится документация к ним. Например посмотрим что есть по ключевому слову `nginx` . Мы видим список образов, а точнее репозиторев в названии которых содержится это слово. Обычно название состоит из двух слов. Первое - имя создателя (аккаунт), второе через слэш - название репозитория. Если слово только одно, это официальный образ от докер. В репозитории может быть несколько версий образов и их имена различаются третьим словом - ом после двоеточия,
например, `bitnami/nginx:1.21.5`, если  (третье сслово) не указан, по умолчанию  - latest, предполагается что это версия по умолчанию.
Все и можно найти на вкладке Tags, а документацию на вкладке Overview. В каждом репозитории хранится как минимум один образ с ом latest.
Создатели бывают - официальный (докер), верифицированные (как правило это корпорации) и простые смертные.
Для начала попробуем запустить официальный образ nginx `docker run -d -p 8080:80 nginx`

4. Но возьмём образ, где всё это уже сделано и сразу же запустим на основе него контейнер `docker run -d -p 8090:80 --name yy yodoim/youtube`. Образ не найден локально и подгружается Docker Hub, причём докер берёт только уникальную информацию относительно того что у нас уже загружено локально. Хранение в Docker организовано по принципу слоёв и новые слои добавляются к уже существующим. Каждый слой может быть использован многократно, но хранится только один раз. Т.е например, если образ сделан на основе nginx, а nginx уже подгружен локально, то подтягивается только слой с изменениями, если конечно совпадают версии nginx. Docker работает на основе UnionFS каскадно-объединённой файловой системой, принцип которой похож на принцип систем контроля версий. Наглядно посмотреть слои можно с помощью сторонней утилиты [dive](https://github.com/wagoodman/dive).
Результат работы данного контейнера смотрим в браузере по адресу http://0.0.0.0:8090.
Давайте тоже создадим свои слои и запишем это в образ.

5. Создавать образ правильнее всего с помощью Dockerfile и инструкций в нём и об этом у нас есть целое отдельное видео! Но сейчас сделаем немного по другому.
Смотрим контейнер в запущенных `docker ps`.
HTML файл хранится тут `/usr/share/nginx/html`. Скачем его себе на локальную машину `docker cp yy:/usr/share/nginx/html/index.html ./index_yodo.html`

6. Изменим файл, например изменим фоновый цвет в стилях `background-color: #49e1d1` и что-нибудь ещё напишем.

7. Теперь все эти изменения загружаем обратно в контейнер.
`docker cp ./index_yodo.html yy:/usr/share/nginx/html/index.html` При конфликте файл просто переписывается, но мы всегда можем загрузить исходных образ ещё раз.

8. Если остановить и удалить этот контейнер ничего не сохранится. Но мы можем запомнить эти изменения в новый образ `docker commit yy yodo_test_my_version`

9. И допустим нам надо передать наш образ другу, коллеге или развернуть на виртуальной машине. Есть два способа: можно всё-таки написать инструкции, подготовить нужные файлы и пусть коллега собирает на своём компе сам, а можно загрузить готовый образ в какое-нибудь хранилище и уже оттуда его подгружать в готовом виде и сразу же разворачивать контейнер, что в случае с виртуалкой сильно предпочтительнее. И самое очевидное хранилище куда можно положить свой образ это докер хаб, тем более что мы уже являемся его пользователями, правда пока что анонимными.

10. Анонимные пользователи могут искать публичные образы, как на сайте, так и с помощью команды `docker search <имя желаемого образа>`, и конечно скачивать их до 100 раз за 6 часов. Иногда этого достаточно, но в нашем случае пора подняться на ступеньку повыше, чтобы иметь возможность закачивать образы и даже сделать свой образ приватным, правда только один и только для себя. https://www.docker.com/pricing - условия по каждому плану.

11. Регистрация

12. Получаем токен. Пользователь -> Account Settings -> Security -> New Access Token . Это пригодится для того чтобы авторизоваться у себя в консоли. Можно настроить права доступа, активировать/деактивировать токен, и всегда можно удалить токен и создать новый. По бесплатному плану одновременно создать можно только один токен. Токен показывается один раз, поэтому его надо куда-нибудь сохранить.

13. Пишем `docker login -u <имя при регистрации>` и когда запрашивается пароль вводим наш токен. Можно конечно и пароль ввести, и тоже будет работать, но это считается небезопасным и в любой момент может быть упразднено.

14. Теперь мы можем залить наш образ в докер хаб. Но для начала нужно поменять название так чтобы оно было по стандартам, иначе образ не загрузится. `docker tag yodo_test_my_version <username>/yodo_youtube`
И теперь можно загружать `docker push <username>/yodo_youtube` Загружается так же по слоям. Дожидаемся сообщения об успешной загрузке.

15. Теперь по адресу https://hub.docker.com/repositories мы можем увидеть наш загруженный образ, кликнуть и посмотреть подробнее. На вкладках General и Tags можно видеть  latest, он создаётся автоматически если ничего не было указано, как в нашем случае. Образ с этим ом создался ещё локально. Если пересоздать образ с тем же ом и загрузить его в хаб, то старый образ просто перепишется. Поэтому если мы планируем сохранять версии, то крайне желательно указывать тэги явно, в том числе тэг latest для образа который мы считаем должен быть скачен по умолчанию. тэги на сайте можно только удалить.

16. Теперь сделаем по фэншую. Добавим  образу тэг
`docker tag <username>/yodo_youtube:latest <username>/yodo_youtube:v1_aquamarine` , в выводе `docker images` можно видеть два образа с одним ID и разными тэгами. ДВобавляем `docker push <username>/yodo_youtube:v1_aquamarine` и `docker push <username>/yodo_youtube:latest`. Мы видим сообщение что все слои уже имеются, но в репозитории у нас есть два тэга. Пока что эти образы абсолютно идентичны, но latest обычно переписывается, а образы с другими тэгами сохраняются и могут быть подгружены при необходимости. Хотя это чисто соглашение и образ с любым тэгом может быть переписан так же как и latest. Это похоже на работу с системой контроля версий, например git.

17. Давайте внесём ещё изменения и сделаем новую версию. Например изменим фоновый цвет в стилях на `background-color: #e0b59a`. Создаём новый образ и изменяем образ по умолчанию.
```
docker cp ./index_yodo.html yy:/usr/share/nginx/html/index.html
echo "v2 palebrown" >> v2.txt
docker cp ./v2.txt yy:/usr/share/nginx/html/v2.txt
docker commit yy <username>/yodo_youtube:v2_palebrown
docker tag <username>/yodo_youtube:v2_palebrown <username>/yodo_youtube:latest
docker push -a <username>/yodo_youtube
```
`docker push -a` - загрузить все образы с этим названием (где тэги указаны, у бывшего latest теперь тэга нет и взаимодействовать с ним можно только по ID)

18. Проверим что получилось. Сотрём все созданные нами образы и запустим контейнер на основе образа из докер хаба. `docker rmi -f` означает стереть даже те образы, на основе которых имеются живые запущенные контейнеры. Контейнер работать будет, но если образ на основе которого он запущен был каким-то уникальным в единственном экземпляре после удаления этого контейнера всё потеряется.
```
docker rmi -f $(docker images -f "reference=<username>/yodo_youtube" -q)
docker run -p 8092:80 <username>/yodo_youtube
```
Открываем бразузер по адресу http://localhost:8082 и фон должен поменяться, а так же доступен адрес http://localhost:8082/v2.txt
Так же всегда можно подгрузить какую-то другую версию, например `docker run -p 8091:80 <username>/yodo_youtube:v1_aquamarine`, проверить http://localhost:8085

19. Так же репозиторий можно скрыть от мира, он не будет находится в результатах поиска докер и не будет доступен для загрузки, всем кроме создателя (если план бесплатный). Для этого на страничке репозитория надо нажать на вкладку Settings, нажать кнопку Make private и подтвердить написав название репозитория (защита). Приватным может быть только один репозиторий при бесплатном плане.

20. Итак, теперь мы знаем основы работы с Docker Hub. Умеем искать и скачивать образы нужной версии, а так же организовывать свои репозитории.
